package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;


public class MyImages extends Activity {


    private GridLayoutManager lLayout;
    public String[] FilePathStrings;
    public String[] FileNameStrings;
    public File[] listFile;
    File file;

    RelativeLayout relativeLayout;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridview_main);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);

        layout1 = (LinearLayout) findViewById(R.id.admob1);
        strip1 = ad.layout_strip(this);
        layout1.addView(strip1);
        ad.AdMobBanner(this);


        // Check for SD Card
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(MyImages.this, "Error! No SDCARD Found!", Toast.LENGTH_LONG).show();
        } else {

            File sdcard = Environment.getExternalStorageDirectory();
            // to this path add a new directory path
            String fullPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + getResources().getString(R.string.app_name)+"/";
            file = new File(fullPath);

            // create this directory if not already created
            file.mkdir();
        }

        if (file.isDirectory()) {
            listFile = file.listFiles();
            FilePathStrings = new String[listFile.length];
            FileNameStrings = new String[listFile.length];

            for (int i = 0; i < listFile.length; i++) {
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                FileNameStrings[i] = listFile[i].getName();
            }
        }

        relativeLayout = (RelativeLayout) findViewById(R.id.outer_relative);


        if ((listFile == null || listFile.length == 0)) {

            relativeLayout.setVisibility(View.INVISIBLE);






        } else {

            relativeLayout.setVisibility(View.VISIBLE);

            lLayout = new GridLayoutManager(MyImages.this, 2);

            RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
            rView.setHasFixedSize(true);
            rView.setLayoutManager(lLayout);

            RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(MyImages.this, FilePathStrings, FileNameStrings);
            rView.setAdapter(rcAdapter);


        }

    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        // private List<ItemObject> itemList;
        private Context context;
        private String[] filepath;
        String[] filename;


        public RecyclerViewAdapter(Context context, String[] fpath, String[] fname) {
            this.context = context;
            filepath = fpath;
            filename = fname;
        }

        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_three_card_view_list, null);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolders holder, int position) {
            try {
                Bitmap bmp = BitmapFactory.decodeFile(filepath[position]);
                holder.countryPhoto.setImageBitmap(bmp);
            } catch (Exception e) {
                // TODO: handle exception
                Toast.makeText(context.getApplicationContext(), "SomeThing Went Wrong Please Go back", Toast.LENGTH_LONG)
                        .show();
            }

        }

        @Override
        public int getItemCount() {
            return this.filepath.length;
        }
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements OnClickListener {

        public ImageView countryPhoto;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            countryPhoto = (ImageView) itemView.findViewById(R.id.country_photo);
        }

        @Override
        public void onClick(View view) {
            // Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(view.getContext(), DisplyImage.class).putExtra("POS", getPosition()));
        }
    }


    public class ItemObject {

        private int photo;

        public ItemObject(int photo) {
            //  this.name = name;
            this.photo = photo;
        }


        public int getPhoto() {
            return photo;
        }

        public void setPhoto(int photo) {
            this.photo = photo;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), Launcher_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }

}