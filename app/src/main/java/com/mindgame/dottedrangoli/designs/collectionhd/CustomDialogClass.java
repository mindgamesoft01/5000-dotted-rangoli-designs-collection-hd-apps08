package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by kalyani on 8/8/2017.
 */

public  class CustomDialogClass extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Dialog d;
    public TextView yes;
    TextView dis;
    int tag;
    Dialog dialog;
    SharedPreferences pref;

    public CustomDialogClass(Activity a, int i) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.tag=i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        dis=(TextView)findViewById(R.id.txt_dis);
        yes = (TextView) findViewById(R.id.txt_ok);


        yes.setOnClickListener(this);
        if(tag==0){
            dis.setText(R.string.page_refresh);

        }
        if(tag==1){
            dis.setText(R.string.network_error);

        }

        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_ok:
                dismiss();
                break;

            default:
                break;
        }
        dismiss();
    }

    public void customDailog() {

// custom dialog

        dialog = new Dialog(c, android.R.style.Theme_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogbox);
        pref = PreferenceManager.getDefaultSharedPreferences(c);

        dialog.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("RATE", true);
                editor.commit();
                dialog.dismiss();
                //Intent i =new Intent(c, StartPage_hd.class);
                //c.startActivity(i);

            }

        });

        dialog.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("RATE", false);
                editor.commit();


                    Intent i = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + c.getApplicationContext().getPackageName()));

                    c.startActivity(i);


                dialog.dismiss();

            }

        });

        dialog.setCancelable(false);

        dialog.show();

    }


}
