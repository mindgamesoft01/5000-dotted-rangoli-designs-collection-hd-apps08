package com.mindgame.dottedrangoli.designs.collectionhd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ImageviewActivity extends Activity {

    CustomPagerAdapter customPagerAdapter;
    CirclePageIndicator indicator;
    ViewPager viewPager;
    TextView indicateimages;
    int val,val1;
    //WallpaperManager wallpaperManager;
    Button wallpaper;
   int image_urls[];
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();


    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);

        indicateimages = (TextView) findViewById(R.id.indicateimages);
        wallpaper=(Button)findViewById(R.id.setaswallpaper);

        Intent i=getIntent();
        image_urls=i.getIntArrayExtra("images");
        //final ArrayList image_Urls = prepareData();

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        customPagerAdapter = new CustomPagerAdapter(this, image_urls);
        viewPager = (ViewPager) findViewById(R.id.viewpager_suit);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(25);

        viewPager.setPadding(45, 0, 45, 0);

        viewPager.setAdapter(customPagerAdapter);
//        indicator = (CirclePageIndicator) findViewById(R.id.pagerIndicator);
//        indicator.setViewPager(viewPager);


        int position = i.getIntExtra("POS", 0);
        viewPager.setCurrentItem(position);
        indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + image_urls.length);

        findViewById(R.id.saveImagebutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bitmap bmp= BitmapFactory.decodeResource(getResources(),image_urls[viewPager.getCurrentItem()]);
                String img_name = viewPager.getCurrentItem() + "_" + image_urls.length;
                saveImageToExternalStorage2(bmp, img_name);

            }

        });
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svaeImage();
                val = 1;
            }

        });
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            // optional
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            // optional
            @Override
            public void onPageSelected(int position) {
            }

            // optional
            @Override
            public void onPageScrollStateChanged(int state) {
                indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + image_urls.length);

            }
        });

        findViewById(R.id.myimags).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyImages.class));
            }
        });

        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));


            }
        });

        wallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                Bitmap bmp= BitmapFactory.decodeResource(getResources(),image_urls[viewPager.getCurrentItem()]);
                try {
                    myWallpaperManager.clear();
                    myWallpaperManager.setBitmap(bmp);
                    Toast.makeText(getApplicationContext(),
                            "Wallpaper Set Successfully!!", Toast.LENGTH_SHORT)
                            .show();

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(),
                            "Setting WallPaper Failed!!", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }

    private void svaeImage() {
        Bitmap bmp= BitmapFactory.decodeResource(getResources(),image_urls[viewPager.getCurrentItem()]);

        shareandSave(bmp);

        if (AppStatus.getInstance(ImageviewActivity.this).isOnline()) {
            //new AsyncTaskLoadImage().execute(imageUrls.get(viewPager.getCurrentItem()));

        } else {
            Toast.makeText(getApplicationContext(), "Please Check NetWork Connection", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
    }


//    public class AsyncTaskLoadImage extends AsyncTask<int i, Bitmap> {
//        private final static String TAG = "AsyncTaskLoadImage";
//        ProgressDialog ringProgressDialog;
//
//        public AsyncTaskLoadImage() {
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            ringProgressDialog = ProgressDialog.show(ImageviewActivity.this, "Please wait ...", "Downloading Image ...", true);
//
//            ringProgressDialog.setCancelable(true);
//
//        }
//
//        @Override
//        protected Bitmap doInBackground(String... params) {
//            Bitmap bitmap = null;
//            try {
//                URL url = new URL(params[0]);
//                bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());
//
//
//            } catch (IOException e) {
//                Log.e(TAG, e.getMessage());
//            }
//            return bitmap;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//
//
//            ringProgressDialog.dismiss();
//
//
//            shareandSave(bitmap);
//
//        }
//    }

    private void shareandSave(Bitmap bmp) {
        if (val == 1) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temp.jpg"));
            try {
                startActivity(Intent.createChooser(share, "Share via"));
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Please Connect To Internet", Toast.LENGTH_LONG)
                        .show();
            }

        } else {
            saveImageToExternalStorage(bmp);
        }
    }



    void saveImageToExternalStorage(Bitmap b) {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = sf.format(new Date());

        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/mehndidesigns/";

        String temp_path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;


        try {
            File dir_temp = new File(temp_path);
            if (!dir_temp.exists()) {
                dir_temp.mkdirs();
            }
            OutputStream fOut_temp = null;
            File file_temp = new File(temp_path, "/mehndidesigns/" + ".jpg");
            if (file_temp.exists())
                file_temp.delete();
            file_temp.createNewFile();
            fOut_temp = new FileOutputStream(file_temp);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut_temp);
            fOut_temp.flush();
            fOut_temp.close();
            Toast.makeText(getApplicationContext(), "Image saved successfully", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            OutputStream fOut = null;
            File file = new File(fullPath, "picture" + ".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

    }

    void saveImageToExternalStorage2(Bitmap b, String img_name) {

        //String fullPath1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),getResources().getString(R.string.app_name);
        String fullPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + getResources().getString(R.string.app_name)+"/";
img_name = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            OutputStream fOut = null;
            File file = new File(fullPath, img_name +".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            Toast.makeText(getApplicationContext(), img_name+" saved successfully", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

    }


}
