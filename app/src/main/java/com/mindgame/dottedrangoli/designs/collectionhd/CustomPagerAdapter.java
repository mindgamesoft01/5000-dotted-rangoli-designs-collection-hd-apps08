package com.mindgame.dottedrangoli.designs.collectionhd;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by MM 9 on 17-Oct-16.
 */

public class CustomPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    public static int val;
    SharedPreferences pref;
    private int []  loadingimags;

    public CustomPagerAdapter(Context context, int []  image_urls) {
        mContext = context;
        this.loadingimags = image_urls;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return loadingimags.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);


        ImageView imageView = (ImageView) itemView.findViewById(R.id.suit_image);
        imageView.setOnTouchListener(new MultiTouchListener());
      //  Picasso.with(mContext).load(loadingimags.get(position).getImage_url()).resize(120, 60).into(imageView);
        Glide.with(mContext).load(loadingimags[position]).into(imageView);

      //  imageView.setOnTouchListener(new TouchImageView());
        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //    mContext.startActivity(new Intent(mContext, .class));
            }
        });*/

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
