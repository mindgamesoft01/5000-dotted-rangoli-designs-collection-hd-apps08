package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DisplyImage extends Activity {
    RelativeLayout r;

    File file, file_temporary;
    File[] listFile;
    int position;
    int pos;
    Bitmap bit;
    ImageButton share, delete, whatsapp;
    ImageView imageview;
    String[] filepath;
    FileOutputStream fo;
    String extStorageDirectory;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();

   ImageButton wallpaper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_image_vertical);
        wallpaper=(ImageButton)findViewById(R.id.wallpaper);


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        //file = new File(Environment.getExternalStorageDirectory(), AdClass.folder_name);

        String fullPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + getResources().getString(R.string.app_name)+"/";
        file = new File(fullPath);

        Intent i = getIntent();
        bit = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        position = i.getIntExtra("POS", 0);

        imageview = (ImageView) findViewById(R.id.image1);

        if (file.isDirectory()) {
            listFile = file.listFiles();
            filepath = new String[listFile.length];
            pos = position;
            filepath[position] = listFile[position].getAbsolutePath();
            bit = BitmapFactory.decodeFile(filepath[position]);
            if (bit != null) {
                imageview.setImageBitmap(bit);
            } else {
                imageview.setBackgroundResource(R.mipmap.ic_launcher);
            }

        }
        wallpaper.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                try {
                    myWallpaperManager.clear();
                    myWallpaperManager.setBitmap(bit);
                    Toast.makeText(getApplicationContext(),
                            "Wallpaper Set Successfully!!", Toast.LENGTH_SHORT)
                            .show();

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(),
                            "Setting WallPaper Failed!!", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });

        findViewById(R.id.text_delete).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                final String imgPath = filepath[pos];
                File file = new File(imgPath);
                file.delete();

                Toast.makeText(getApplicationContext(), "File Deleted", Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), MyImages.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            }
        });


        findViewById(R.id.share).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.jpg");
                try {
                    f.createNewFile();
                    fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temp.jpg"));
                share.putExtra(Intent.EXTRA_TEXT, " " + "\n This image is edited using .. " + "\n"
                        + "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                try {
                    startActivity(Intent.createChooser(share, "Share via"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Please Connect To Internet", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });


    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MyImages.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }
}
