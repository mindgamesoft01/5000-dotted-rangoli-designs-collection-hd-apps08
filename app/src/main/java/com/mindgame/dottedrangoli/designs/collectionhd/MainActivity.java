package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {





    public static int[] image_urls_a1=ListImages.image_urls_a1;
    public static int[] image_urls_two=ListImages.image_urls_two;
    public static int[] image_urls_three=ListImages.image_urls_three;
    public static int[] image_urls_four=ListImages.image_urls_four;
    public static int[] image_urls_five=ListImages.image_urls_five;
    public static int[] image_urls_six=ListImages.image_urls_six;
    public static int[] image_urls_seven=ListImages.image_urls_seven;
    public static int[] image_urls_eight=ListImages.image_urls_eight;
    public static int[] image_urls_nine=ListImages.image_urls_nine;
    public static int[] image_urls_ten=ListImages.image_urls_ten;
    public static int[] image_urls_eleven=ListImages.image_urls_eleven;
    public static int[] image_urls_twelve=ListImages.image_urls_twelve;







    int val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    String cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle b=getIntent().getExtras();
        val = getIntent().getExtras().getInt("flag");
        cat=getIntent().getExtras().getString("cat");


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        initViews();
    }

    private void initViews() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        heading = (TextView) findViewById(R.id.heading_text);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        if(cat.equals("one")){
            heading.setText(R.string.one);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_a1);
            recyclerView.setAdapter(adapter);

        }


        if(cat.equals("two")){
            heading.setText(R.string.two);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_two);
            recyclerView.setAdapter(adapter);

        }
       if(cat.equals("three")){
           heading.setText(R.string.three);
           DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_three);
           recyclerView.setAdapter(adapter);

       }
       if(cat.equals("four")){

           heading.setText(R.string.four);
           DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_four);
           recyclerView.setAdapter(adapter);


       }
        if(cat.equals("five")){

            heading.setText(R.string.five);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_five);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("six")){

            heading.setText(R.string.six);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_six);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("seven")){

            heading.setText(R.string.seven);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_seven);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("eight")){

            heading.setText(R.string.eight);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_eight);
            recyclerView.setAdapter(adapter);


        }

        if(cat.equals("nine")){

            heading.setText(R.string.nine);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_nine);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("ten")){

            heading.setText(R.string.ten);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_ten);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("eleven")){

            heading.setText(R.string.eleven);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_eleven);
            recyclerView.setAdapter(adapter);


        }
        if(cat.equals("twelve")){

            heading.setText(R.string.tweleve);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_twelve);
            recyclerView.setAdapter(adapter);


        }



    }


}
