package com.mindgame.dottedrangoli.designs.collectionhd;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;


public class Launcher_Activity extends AppCompatActivity {
    Button gallery,favourite,rate;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    TextView privacypolicy;
    singleton_images sc=singleton_images.getInstance();
    String app_status = sc.app_mode();
    LinearLayout fav_layout,rate_layout;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {/* ... */}
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();

        setContentView(R.layout.activity_launcher);
        init();
        this.context=getApplicationContext();


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);

        layout1 = (LinearLayout) findViewById(R.id.admob1);
        strip1 = ad.layout_strip(this);
        layout1.addView(strip1);
        ad.AdMobBanner(this);
        if (app_status.equals("TEST")) {

            Intent i = new Intent(Launcher_Activity.this, TestActivity.class);
            startActivity(i);
        }

        if (app_status.equals("PROD"))
        {

            gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Launcher_Activity.this, Start_Before_Page.class);
                    startActivity(i);
                }
            });
            favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Launcher_Activity.this, MyImages.class);
                    startActivity(i);
                }
            });
            privacypolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Launcher_Activity.this, PrivacyPolicy.class);
                    startActivity(i);
                }
            });
            findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                    }

                }
            });
        }
    }
    public void init(){
        gallery=(Button)findViewById(R.id.gallery);
        favourite=(Button)findViewById(R.id.favourite);
        rate=(Button)findViewById(R.id.rate);
        privacypolicy=(TextView)findViewById(R.id.privacy);
        fav_layout=(LinearLayout)findViewById(R.id.fav_layout);
        rate_layout=(LinearLayout)findViewById(R.id.rate_layour);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return super.onKeyDown(keyCode, event);
    }
}
