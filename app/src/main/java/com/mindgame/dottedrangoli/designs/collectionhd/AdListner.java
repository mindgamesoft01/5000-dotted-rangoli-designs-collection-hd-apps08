package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by lenovo on 07-May-17.
 */

public class AdListner {

    InterstitialAd interstitial;
    String AD_UNIT_ID_full_page = "  ";

    ProgressDialog ringProgressDialog;

    public void AdMobInterstitial(final Context c, final Class cla, final int pos) {
        interstitial = new InterstitialAd(c);
        interstitial.setAdUnitId(AD_UNIT_ID_full_page);
        ringProgressDialog = ProgressDialog.show(c, "Please wait ...", "Loading ...", true);

        ringProgressDialog.setCancelable(true);


        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // TODO Auto-generated method stub
                super.onAdLoaded();
                interstitial.show();
                ringProgressDialog.dismiss();
                c.startActivity(new Intent(c,cla).putExtra("POS",pos));
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                ringProgressDialog.dismiss();
                c.startActivity(new Intent(c,cla).putExtra("POS",pos));
            }

        });

    }
}
