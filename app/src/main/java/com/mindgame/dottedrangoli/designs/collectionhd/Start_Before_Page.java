package com.mindgame.dottedrangoli.designs.collectionhd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

public class Start_Before_Page extends AppCompatActivity {
    ImageView one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,ninteen,twenty;
    TextView txt_1,txt_2,txt_3,txt_4,txt_5,txt_6,txt_7,txt_8,txt_9,txt_10,txt_11,txt_12,txt_13,txt_14,txt_15,txt_16,txt_17,txt_18,txt_19,txt_20;
    ScrollView scrollView;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/9420346563";
//    String AD_UNIT_ID_full_page = "ca-app-pub-3940256099942544/1033173712";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/9420346563";
//    String AD_UNIT_ID_full_page1 = "ca-app-pub-3940256099942544/1033173712";
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();
        initViews();

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","one");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","one");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","one");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","one");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });


        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","two");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","two");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","two");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","two");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","three");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","three");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","three");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","three");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","four");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","four");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","four");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","four");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });


        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","five");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","five");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","five");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","five");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","six");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","six");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","six");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","six");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","seven");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","seven");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","seven");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","seven");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","eight");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","eight");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","eight");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","eight");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","nine");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","nine");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","nine");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","nine");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
        ten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","ten");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","ten");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","ten");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","ten");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });

        eleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","eleven");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","eleven");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","eleven");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","eleven");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });
//
//        twelve.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //interstitialAd.setAdUnitId(interstitial0);
//
//                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
//                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    Bundle b=new Bundle();
//                    b.putString("cat","twelve");
//                    i.putExtras(b);
//                    startActivity(i);
//                    pd.dismiss();
//                }
//                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//
//                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
//                    pd.show();
//                    Log.e(TAG, ad1_status.toString());
//                    if (ad1_status == Boolean.FALSE) {
//                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                        Bundle b=new Bundle();
//                        b.putString("cat","twelve");
//                        i.putExtras(b);
//                        startActivity(i);
//                        pd.dismiss();
//
//
//                    }
//
//
//                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//                        interstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
//                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","twelve");
//                                i.putExtras(b);
//                                startActivity(i);
//                                pd.dismiss();
//
//
//                            }
//
//                            @Override
//                            public void onAdFailedToLoad(int i) {
//                                super.onAdFailedToLoad(i);
//
//                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                                    //cd.show();
//                                    pd.dismiss();
//                                }
//                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","twelve");
//                                j.putExtras(b);
//                                startActivity(j);
//                                pd.dismiss();
//
//                                //set the last loaded timestamp even if the ad load fails
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                            }
//
//                            @Override
//                            public void onAdLeftApplication() {
//                                super.onAdLeftApplication();
//                            }
//
//                            @Override
//                            public void onAdOpened() {
//                                super.onAdOpened();
//                            }
//
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                //set the last loaded timestamp
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                                interstitialAd.show();
//                                pd.dismiss();
//
//                            }
//                        });
//                        interstitialAd.loadAd(adRequest);
//                    }
//                }
//            }
//        });

    }
    public void initViews(){
        one=(ImageView) findViewById(R.id.one);
        two=(ImageView) findViewById(R.id.two);
        three=(ImageView) findViewById(R.id.three);
        four=(ImageView) findViewById(R.id.four);
        five=(ImageView) findViewById(R.id.five);
        six=(ImageView) findViewById(R.id.six);
       seven=(ImageView) findViewById(R.id.seven);
        eight=(ImageView) findViewById(R.id.eight);
        nine=(ImageView) findViewById(R.id.nine);
        ten=(ImageView) findViewById(R.id.ten);
       eleven=(ImageView) findViewById(R.id.eleven);
//        twelve=(ImageView) findViewById(R.id.twelve);
//        thirteen=(ImageView) findViewById(R.id.thirteen);
//        fourteen=(ImageView) findViewById(R.id.fourteen);
//        fifteen=(ImageView) findViewById(R.id.fifteen);
//        sixteen=(ImageView) findViewById(R.id.sixteen);
//        seventeen=(ImageView) findViewById(R.id.seventeen);
//        eighteen=(ImageView) findViewById(R.id.eighteen);
//        ninteen=(ImageView) findViewById(R.id.ninteen);
//        twenty=(ImageView) findViewById(R.id.twenty);
//        one.setBackgroundResource(R.drawable.one);
//        two.setBackgroundResource(R.drawable.two);
//        three.setBackgroundResource(R.drawable.three);
//        four.setBackgroundResource(R.drawable.four);
//        five.setBackgroundResource(R.drawable.five);
//        six.setBackgroundResource(R.drawable.six);
//        seven.setBackgroundResource(R.drawable.seven);
//        eight.setBackgroundResource(R.drawable.eight);
//        nine.setBackgroundResource(R.drawable.nine);
//        ten.setBackgroundResource(R.drawable.ten);
//        eleven.setBackgroundResource(R.drawable.eleven);
//        twelve.setBackgroundResource(R.drawable.twelve);
//        thirteen.setBackgroundResource(R.drawable.thirteen);
//        fourteen.setBackgroundResource(R.drawable.fourteen);
//        fifteen.setBackgroundResource(R.drawable.fifteen);
//        sixteen.setBackgroundResource(R.drawable.sixteen);
//        seventeen.setBackgroundResource(R.drawable.seventeen);
//        eighteen.setBackgroundResource(R.drawable.eighteen);
//        ninteen.setBackgroundResource(R.drawable.nineteen);
//        twenty.setBackgroundResource(R.drawable.twenty);


        scrollView=(ScrollView)findViewById(R.id.scrollView);

        txt_1=(TextView)findViewById(R.id.txt_1);
        txt_2=(TextView)findViewById(R.id.txt_2);
        txt_3=(TextView)findViewById(R.id.txt_3);
        txt_4=(TextView)findViewById(R.id.txt_4);
        txt_5=(TextView)findViewById(R.id.txt_5);
        txt_6=(TextView)findViewById(R.id.txt_6);
        txt_7=(TextView)findViewById(R.id.txt_7);
        txt_8=(TextView)findViewById(R.id.txt_8);
       txt_9=(TextView)findViewById(R.id.txt_9);
        txt_10=(TextView)findViewById(R.id.txt_10);
       txt_11=(TextView)findViewById(R.id.txt_11);
//       txt_12=(TextView)findViewById(R.id.txt_12);
//        txt_13=(TextView)findViewById(R.id.txt_13);
//        txt_14=(TextView)findViewById(R.id.txt_14);
//        txt_15=(TextView)findViewById(R.id.txt_15);
//        txt_16=(TextView)findViewById(R.id.txt_16);
//        txt_17=(TextView)findViewById(R.id.txt_17);
//        txt_18=(TextView)findViewById(R.id.txt_18);
//        txt_19=(TextView)findViewById(R.id.txt_19);
//        txt_20=(TextView)findViewById(R.id.txt_20);

        txt_1.setText(R.string.one);
        txt_2.setText(R.string.two);
        txt_3.setText(R.string.three);
        txt_4.setText(R.string.four);
        txt_5.setText(R.string.five);
        txt_6.setText(R.string.six);
        txt_7.setText(R.string.seven);
        txt_8.setText(R.string.eight);
        txt_9.setText(R.string.nine);
        txt_10.setText(R.string.ten);
        txt_11.setText(R.string.eleven);
//       txt_12.setText(R.string.tweleve);
//        txt_13.setText(R.string.thirteen);
//        txt_14.setText(R.string.fifteen);
//        txt_15.setText(R.string.fifteen);
//        txt_16.setText(R.string.sixteen);
//        txt_17.setText(R.string.seventeen);
//        txt_18.setText(R.string.eighteen);
//        txt_19.setText(R.string.nineteen);
//        txt_20.setText(R.string.twenty);



    }
}
